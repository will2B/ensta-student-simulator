#from visitor import Visitor

class Ast: #classe AST dont heritent toutes les sous-classes composant l'AST
    def __init__(self, name): #constructeur
        self.name = name

    # fonction qui utilise la metaprogrammation pour appeler les fonctions du visiteur
    def accept(self, visitor, arg):
        nom = self.__class__.__name__
        nomMethode = getattr(visitor, "visit" + nom)
        nomMethode(self, arg)


#toutes les classes seront construites sur le meme modele que celle-ci
class Personnage(Ast):
    def __init__(self, nom):#, position = [0,0], action):
        self.nom = nom;

    def __str__(self):
        return "    personnage - {}".format(self.name);

class Nom(Ast):
    def __init__(self, nom):
        self.nom = nom;

    def __str__(self):
        return "           nom - {}".format(self.name);

class Position(Ast):
    def __init__(self, nom):
        self.nom = nom;

    def __str__(self):
        return "      position - {}".format(self.name);

class Action(Ast):
    def __init__(self, nom):
        self.nom = nom;

    def __str__(self):
        return "        action - {}".format(self.name);

class MotDeLiaison(Ast):
    def __init__(self, nom):#,implication):
        self.nom = nom;

    def __str__(self):
        return "mot de liaison - {}".format(self.name);

class Implication(Ast):
    def __init__(self, nom):#,implication):
        self.nom = nom;

    def __str__(self):
        return "mot de liaison - {}".format(self.name);

#------------------------------------------------------------------------------#
"""
class Main(Ast):
    def __init__(self):
        self.type1 = None
        self.ident = []
        self.type2 = []
        self.statements = []

    def __str__(self):
        string = "Main\n {0}".format(self.type1)
        for i in range(len(self.ident)) :
        	string = string + "\n : {0}".format(self.type2[i])
        	string = string + "\n : {0}".format(self.ident[i])
        for stmt in self.statements :
        	string = string + "\n statement : {0}".format(stmt)
        return string
"""
class Racine(Ast):
    def __init__(self):
        self.includes = []
        self.raccourcis = []
        self.main = []

    def __str__(self):
    	print "---------------------------AST-----------------------------"
        string = "Ast"
        for include in self.includes :
        	string = string + "\n {0}".format(include)
        for raccourci in self.raccourcis :
        	string = string + "\n {0}".format(raccourci)
        string = string + "\n {0}".format(self.main)
        return string
