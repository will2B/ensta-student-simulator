# -*- coding: utf-8 -*-
"""
Created on Thu May 26 01:55:11 2016

@author: yoann
"""

from tkinter import *
from lieux import *
from personnages import *
from main import *
import time

listepersonnages = CustomList()
generation(listepersonnages,20)
print(listepersonnages)
toilettes=Stand("toilettes",((20,0),(25,5)),10,0,True)
bar=Consommation("bar",((0,25),(24,30)),20,5,0,True)
buffet=Consommation("buffet",((25,0),(30,30)),20,5,0,True)  
piste=Stand("piste",((0,1),(19,25)),100,0,True)
sortie=Stand("sortie",((0,0),(1,0)),10,0,True)
localisation=Localisation(30,30)
localisation.actualisationstand(toilettes)
localisation.actualisationstand(buffet)
localisation.actualisationstand(bar)
localisation.actualisationstand(piste)
localisation.actualisationstand(sortie)
print(localisation)
salle=Salle(30,30)



def unitour():
    can.delete(ALL)
    untour(toilettes,buffet,bar,piste,sortie,localisation,salle,listepersonnages)
    for i in range(31):
        for j in range(31):
            if salle.table[i,j]:
                image=can.create_image(0, 0, anchor=NW, image=photo)
                can.itemconfig(image,tags=('bg,'))
                can.tag_lower('bg,')
                can.create_oval(30*(j+2)-5,30*(i+2)-5,30*(j+2)+5,30*(i+2)+5,fill='red')
                can.pack()

def simu():
    can.delete(ALL)
    compteur=0
    while listepersonnages.first.next is not None:
        untour(toilettes,buffet,bar,piste,sortie,localisation,salle,listepersonnages)
        can.delete(ALL)
        for i in range(31):
            for j in range(31):
                if salle.table[i,j]:
                    image=can.create_image(0, 0, anchor=NW, image=photo)
                    can.itemconfig(image,tags=('bg,'))
                    can.tag_lower('bg,')
                    can.create_oval(30*(j+2)-5,30*(i+2)-5,30*(j+2)+5,30*(i+2)+5,fill='red')
                    can.pack()
                    fenetre.update()
        compteur+=1
        print("{} tours effectués au total".format(compteur))
        time.sleep(2)
        if compteur>=50:
            p=listepersonnages.first
            while p.next is not None:
                p=p.next
                p.etat="depart"
   
        

        
    
fenetre=Tk()
photo = PhotoImage(file="fond.png")
can =Canvas(fenetre,width=950,height=950,bg='white')
can.create_image(0, 0, anchor=NW, image=photo)
can.pack(side=BOTTOM,pady=20)
bouton=Button(fenetre,text='un tour',command=unitour)
bouton.pack(side=LEFT,padx=100)
bouton2=Button(fenetre,text='simulation',command=simu)
bouton2.pack(side=RIGHT,padx=100)
fenetre.mainloop()