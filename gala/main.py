# -*- coding: utf-8 -*-
"""
Created on Wed Mar 23 10:46:15 2016

@author: Benjamin
"""
from lieux import *
from personnages import *
import random as rdm
import time



def evolution(perso1,toilettes,buffet,bar,piste,sortie,localisation,salle,listepersonnages):
    '''fait evoluer le personnage en fonction de son etat et de l'etat des stands'''
    etat1=perso1.etat
    if etat1=="vouloiruriner":
        if perso1.isin(toilettes):
            if toilettes.occupants<toilettes.capacite:
                perso1.etat="uriner"
                toilettes.arriveeclient()
        else:
            perso1.marcher(toilettes,salle)
            salle.actualisation(listepersonnages)
    elif etat1=="uriner":
        perso1.uriner()
        toilettes.departclient()
    elif etat1=="repos":
        t=rdm.random()
        if t<=0.5:
            perso1.etat="vouloirmanger"
        else:
            perso1.etat="vouloirboire"
    elif etat1=="depart":
        perso1.partir(sortie,listepersonnages,salle)
    elif etat1=="manger":
        if buffet.etat==False:
            perso1.etat="inactif"
        else:
            perso1.manger(buffet,localisation)        
    elif etat1=="vouloirmanger":
        if buffet.etat==False:
            perso1.etat="inactif"
        elif perso1.isin(buffet):
            if buffet.occupants<buffet.capacite:
                perso1.etat="manger"
                buffet.arriveeclient()
        else:
            perso1.marcher(buffet,salle)
            salle.actualisation(listepersonnages)
    elif etat1=="boire":
        if bar.etat==False or perso1.argent<1:
            perso1.etat="inactif"
        else:
            perso1.boire(bar,localisation)
    elif etat1=="vouloirboire":
        if bar.etat==False or perso1.argent<1:
            perso1.etat="inactif"
        elif perso1.isin(bar):
            if bar.occupants<bar.capacite:
                perso1.etat="boire"
                bar.arriveeclient()
        else:
            perso1.marcher(bar,salle)
            salle.actualisation(listepersonnages)
    elif etat1=="danser":
        perso1.danser()
    elif etat1=="vouloirdanser":
        if perso1.isin(piste):
            perso1.etat="danser"
        else:
            perso1.marcher(piste,salle)
            salle.actualisation(listepersonnages)
    elif etat1=="vouloirparler":
        n=listepersonnages.search(perso1.parle2)
        p=listepersonnages.first
        p=p.next
        if n is not None:
            for i in range(n):
                p=p.next
            if perso1.isaround(perso1.parle2,salle):
                if p.etat=="parler":
                    perso1.etat="inactif"
                    perso1.parle1=0
                    perso1.parle2=0
                    perso1.parle3=0
                elif p.etat=="vouloirparler":
                    if p.parle2==perso1.numero:
                        p.etat="parler"
                        p.parle1=1
                        p.parle2=perso1.numero
                        p.parle3=0
                        perso1.etat="parler"
                        perso1.parle1=1
                        perso1.parle2=p.numero
                        perso1.parle3=0
                    else:
                        perso1.etat="inactif"
                        perso1.parle=(0,0,0)
                else:
                    p.etat="parler"
                    p.parle1=1
                    p.parle2=perso1.numero
                    p.parle3=0
                    perso1.etat="parler"
                    perso1.parle1=1
                    perso1.parle2=p.numero
                    perso1.parle3=0
            else:
                perso1.marcher(p,salle)
                salle.actualisation(listepersonnages)
        else:
            perso1.etat="inactif"
    elif etat1=="parler":
        n=listepersonnages.search(perso1.parle2)
        p=listepersonnages.first
        p=p.next
        for i in range(n):
            p=p.next
        perso1.parler(p)
        if perso1.parle3>=10:
            if perso1.couple1==0 and p.couple1==0:
                perso1.couple1=1
                perso1.couple2=p.numero
                p.couple1=1
                p.couple2=perso1.numero
    else:
        if perso1.vessie>16:
            perso1.etat="vouloiruriner"
        elif perso1.depart<=0:
            perso1.etat="depart"
        elif perso1.type=="dragueur":
            t=rdm.random()
            if t<=0.6:
                x=perso1.chercher(salle)
                if x:
                    perso1.parle2=x
                    n=listepersonnages.search(perso1.parle2)
                    p=listepersonnages.first
                    p=p.next
                    for i in range(n):
                        p=p.next
                    perso1.marcher(p,salle)
                    salle.actualisation(listepersonnages)
                    perso1.etat="vouloirparler"
                else:
                    perso1.etat="inactif"
                    evolution(perso1,toilettes,buffet,bar,piste,sortie,localisation,salle,listepersonnages)
            elif t<=0.73:
                if buffet.etat==False:
                    evolution(perso1,toilettes,buffet,bar,piste,sortie,localisation,salle,listepersonnages)
                else:
                    perso1.etat="vouloirmanger"
            elif t<=0.87:
                perso1.etat="vouloirdanser"
            else:
                if bar.etat==False or perso1.argent<1:
                    evolution(perso1,toilettes,buffet,bar,piste,sortie,localisation,salle,listepersonnages)
                else:
                    perso1.etat="vouloirboire"
        elif perso1.type=="danseur":
            t=rdm.random()
            if t<=0.4:
                perso1.etat="vouloirdanser"
            elif t<=0.6:
                if buffet.etat==False:
                    evolution(perso1,toilettes,buffet,bar,piste,sortie,localisation,salle,listepersonnages)
                else:
                    perso1.etat="vouloirmanger"
            elif t<=0.8:
                x=perso1.chercher(salle)
                if x:
                    perso1.parle2=x
                    n=listepersonnages.search(perso1.parle2)
                    p=listepersonnages.first
                    p=p.next
                    for i in range(n):
                        p=p.next
                    perso1.marcher(p,salle)
                    salle.actualisation(listepersonnages)
                    perso1.etat="vouloirparler"
                else:
                    perso1.etat="inactif"
                    evolution(perso1,toilettes,buffet,bar,piste,sortie,localisation,salle,listepersonnages)
            else:
                if bar.etat==False or perso1.argent<1:
                    evolution(perso1,toilettes,buffet,bar,piste,sortie,localisation,salle,listepersonnages)
                else:
                    perso1.etat="vouloirboire"
        elif perso1.type=="buveur":
            t=rdm.random()
            if t<=0.4:
                if bar.etat==False or perso1.argent<1:
                    evolution(perso1,toilettes,buffet,bar,piste,sortie,localisation,salle,listepersonnages)
                else:
                    perso1.etat="vouloirboire"
            elif t<=0.6:
                if buffet.etat==False:
                    evolution(perso1,toilettes,buffet,bar,piste,sortie,localisation,salle,listepersonnages)
                else:
                    perso1.etat="vouloirmanger"
            elif t<=0.8:
                x=perso1.chercher(salle)
                if x:
                    perso1.parle2=x
                    n=listepersonnages.search(perso1.parle2)
                    p=listepersonnages.first
                    p=p.next
                    for i in range(n):
                        p=p.next
                    perso1.marcher(p,salle)
                    salle.actualisation(listepersonnages)
                    perso1.etat="vouloirparler"
                else:
                    perso1.etat="inactif"
                    evolution(perso1,toilettes,buffet,bar,piste,sortie,localisation,salle,listepersonnages)
            else:
                perso1.etat="vouloirdanser"
        elif perso1.type=="affame":
            t=rdm.random()
            if t<=0.4:
                if buffet.etat==False:
                    evolution(perso1,toilettes,buffet,bar,piste,sortie,localisation,salle,listepersonnages)
                else:
                    perso1.etat="vouloirmanger"
            elif t<=0.6:
                perso1.etat="vouloirdanser"
            elif t<=0.8:
                x=perso1.chercher(salle)
                if x:
                    perso1.parle2=x
                    n=listepersonnages.search(perso1.parle2)
                    p=listepersonnages.first
                    p=p.next
                    for i in range(n):
                        p=p.next
                    perso1.marcher(p,salle)
                    salle.actualisation(listepersonnages)
                    perso1.etat="vouloirparler"
                else:
                    perso1.etat="inactif"
                    evolution(perso1,toilettes,buffet,bar,piste,sortie,localisation,salle,listepersonnages)
            else:
                if bar.etat==False or perso1.argent<1:
                    evolution(perso1,toilettes,buffet,bar,piste,sortie,localisation,salle,listepersonnages)
                else:
                    perso1.etat="vouloirboire"
    perso1.depart-=1        
    print(perso1)
    print(salle)


def generation(listepersonnages,n):
    for p in range(n):
        num=0
        p=listepersonnages.first
        while p.next is not None:
            p=p.next
            num=p.numero+1
        sexe=rdm.randint(1,2)
        typer=rdm.randint(1,4)
        if typer==1:
            typer="dragueur"
        elif typer==2:
            typer="buveur"
        elif typer==3:
            typer="danseur"
        else:
            typer="affame"
        a1=rdm.randint(0,9)
        a2=rdm.randint(0,9)
        b1=rdm.randint(0,9)
        b2=rdm.randint(0,9)
        c1=rdm.randint(0,9)
        c2=rdm.randint(0,9)
        d1=rdm.randint(0,9)
        d2=rdm.randint(0,9)
        e1=rdm.randint(0,9)
        e2=rdm.randint(0,9)
        listepersonnages.append(num,typer,rdm.randint(30,40),((min(a1,a2),max(a1,a2)),(min(b1,b2),max(b1,b2)),(min(c1,c2),max(c1,c2)),(min(d1,d2),max(d1,d2)),(min(e1,e2),max(e1,e2))),
                                rdm.randint(0,9),rdm.randint(0,9),rdm.randint(0,9),rdm.randint(0,9),rdm.randint(0,9),rdm.randint(0,9),sexe,rdm.randint(10,20))
        
        

def untour(toilettes,buffet,bar,piste,sortie,localisation,salle,listepersonnages):
    p=listepersonnages.first
    while p.next is not None:
        time.sleep(0.1)
        p=p.next
        evolution(p,toilettes,buffet,bar,piste,sortie,localisation,salle,listepersonnages)
    if rdm.random()<0.2:
        generation(listepersonnages,rdm.randint(2,6))
    if rdm.random()<0.1:
        buffet.ravitaillement(5)
        buffet.ouverture()
        bar.ravitaillement(5) 
        bar.ouverture()
    print("un tour effectué")

def simulation(toilettes,buffet,bar,piste,sortie,localisation,salle,listepersonnages):
    compteur=0
    while listepersonnages.first.next is not None:
        untour(toilettes,buffet,bar,piste,sortie,localisation,salle,listepersonnages)
        compteur+=1
        if compteur>=50:
            p=listepersonnages.first
            while p.next is not None:
                p=p.next
                p.etat="depart"
    print ("simulation finie")



listepersonnages = CustomList()
generation(listepersonnages,20)
print(listepersonnages)
toilettes=Stand("toilettes",((20,0),(25,5)),10,0,True)
bar=Consommation("bar",((0,25),(25,30)),20,50,0,True)
buffet=Consommation("buffet",((25,0),(30,30)),20,50,0,True)  
piste=Stand("piste",((0,1),(19,25)),100,0,True)
sortie=Stand("sortie",((0,0),(1,0)),10,0,True)
localisation=Localisation(30,30)
localisation.actualisationstand(toilettes)
localisation.actualisationstand(buffet)
localisation.actualisationstand(bar)
localisation.actualisationstand(piste)
localisation.actualisationstand(sortie)
print(localisation)
salle=Salle(30,30)
untour(toilettes,buffet,bar,piste,sortie,localisation,salle,listepersonnages)
