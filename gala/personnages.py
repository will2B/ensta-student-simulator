# -*- coding: utf-8 -*-
"""
Created on Sat Feb 27 14:13:21 2016

@author: yoann
"""

import random as rdm

#==============================================================================
#Personnages
#==============================================================================

class personnage():
    """classe qui sert à définir toutes les caractéristiques des personnages
    comme leurs tailles, sexes, poids, niveau de culture etc... qui seront fixes
    ainsi que des caractéristiques variables comme la fatigue, soif, l'argent restant"""
    
    def __init__(self,numero,typer,depart,cherche,taille,humour,beaute,sportivite,culture,sociabilite,sexe,argent,position=(0,0),ebriete=0,fatigue=0,vessie=0,faim=5,soif=5):
        """on initialise avec les paramètres rentrés par l'utilisateur"""
        self.numero=numero  #correspond à l'identité du personnage plutôt que de mettre nom,prénom [0]
        self.type=typer #buveur,dragueur, danseur, affamé 
        self.depart=depart #definit le temps restant avant le départ du personnage  [1]
        self.cherche=cherche #tuple avec les intervalles de valeur des caractéristiques souhaitées chez l'autre (taille,humour,beauté,sport,culture) [2]
        self.taille=taille #de 0 à 9    [3]
        self.humour=humour #de 0 à 9    [4]
        self.beaute=beaute #de 0 à 9    [5]
        self.sport=sportivite #de 0 à 9  [6]
        self.culture=culture #de 0 à 9  [7]
        self.social=sociabilite #de 0 à 9  [8]
        self.sexe=sexe # 1 pour garçon, 2 pour fille   [9]
        self.couple1=0 #tuple [0,0] si celib, [1,num] si en couple avec num le numero du partenaire        [10]
        self.couple2=0
        self.argent=argent #int     [11]
        self.positionx=position[0] #coordonnées [x,y]   [12]
        self.positiony=position[1]
        self.ebriete=ebriete #de 0 à 25        [13]
        self.fatigue=fatigue #de 0 à 50        [14]
        self.vessie=vessie #de 0 à 25          [15]
        self.faim=faim #de 0 à 50         [16]
        self.soif=soif #de 0 à 50         [17]
        self.etat="inactif"  #définit les actions à effectuer après :(manger,boire,danser,uriner,parler,repos)       [18]
        self.parle1=0   #[0,0,0] si ne parle pas,[0,num,0] quand veut parler à num [1,num,tps] quand parle avec num depuis tps tour            [19]
        self.parle2=0
        self.parle3=0        
        self.next = None  # next est un mot clé
        
    def __str__(self):
        return "\n\tThis node contains person whose number and state are:\n\t\t"\
            + str(self.numero) + " " + str(self.etat)
        
    def marcher(self,objectif,salle):
        '''déplacement des personnages vers l'objectif'''
        x=self.positionx
        y=self.positiony
        if objectif.numero=="buffet" or objectif.numero=="toilettes" or objectif.numero=="bar" or objectif.numero=="piste" or objectif.numero=="sortie":
            if (x>=objectif.position[0][0]) and (x<=objectif.position[1][0]):   #le personnage est dans le bon axe vertical, il n' a plus qu'à monter ou descendre
                if y>objectif.position[1][1]:   #le personnage doit monter
                    for i in range(max(0,5-y),5):    #si proche du mur du haut
                        for j in range(5):  
                            if salle.table[y-5+i][x+min(j,salle.longueur-x)]==0:   #la case est libre
                                self.positionx=x+min(j,salle.longueur-x)
                                self.positiony=y-5+i
                                return (True)
                            elif salle.table[y-5+i][x-min(j,x)]==0:
                                self.positionx=x-min(j,x)
                                self.positiony=y-5+i
                                return (True)
                elif y<objectif.position[0][1]:   #le personnage doit descendre
                    for i in range(max(0,5-(salle.hauteur-y)),5):    #si proche du mur du bas
                        for j in range(5):  
                            if salle.table[y+5-i][x+min(j,salle.longueur-x)]==0:   #la case est libre
                                self.positionx=x+min(j,salle.longueur-x)
                                self.positiony=y+5-i
                                return (True)
                            elif salle.table[y+5-i][x-min(j,x)]==0:
                                self.positionx=x-min(j,x)
                                self.positiony=y+5-i
                                return (True)
            elif (y>=objectif.position[0][1]) and (y<=objectif.position[1][1]):   #le personnage est dans le bon axe horizontal, il n' a plus qu'à aller à gauche ou à droite
                if x>objectif.position[1][0]:   #le personnage doit aller à gauche
                    for i in range(max(0,5-x),5):    #si proche du mur de gauche
                        for j in range(5):  
                            if salle.table[y+min(j,salle.hauteur-y)][x-5+i]==0:   #la case est libre
                                self.positionx=x-5+i
                                self.positiony=y+min(j,salle.hauteur-y)
                                return (True)
                            elif salle.table[y-min(j,y)][x-5+i]==0:
                                self.positionx=x-5+i
                                self.positiony=y-min(j,y)
                                return (True)
                elif x<objectif.position[0][0]:   #le personnage doit aller à droite
                    for i in range(max(0,5-(salle.longueur-x),5)):    #si proche du mur de droite
                        for j in range(5):  
                            if salle.table[y+min(j,salle.hauteur-y)][x+5-i]==0:   #la case est libre
                                self.positionx=x+5-i
                                self.positiony=y+min(j,salle.hauteur-y)
                                return (True)
                            elif salle.table[y-min(j,y)][x+5-i]==0:
                                self.positionx=x+5-i
                                self.positiony=y-min(j,y)
                                return (True)
            else:
                if (x>objectif.position[1][0]) and (y>objectif.position[1][1]):   #le personnage doit aller en haut à gauche
                    for i in range(max(0,5-x),5):    #si proche du mur de gauche
                        for j in range(max(0,5-y),5):  #si proche du mur du haut
                            if salle.table[y-5+j][x-5+i]==0:   #la case est libre
                                self.positionx=x-5+i
                                self.positiony=y-5+j
                                return (True)
                elif (x<objectif.position[0][0]) and (y>objectif.position[1][1]):   #le personnage doit aller en haut à droite
                    for i in range(max(0,5-(salle.longueur-x)),5):    #si proche du mur de droite
                        for j in range(max(0,5-y),5):  #si proche du mur du haut
                            if salle.table[y-5+j][x+5-i]==0:   #la case est libre
                                self.positionx=x+5-i
                                self.positiony=y-5+j
                                return (True)
                elif (x>objectif.position[1][0]) and (y<objectif.position[0][1]):   #le personnage doit aller en bas à gauche
                    for i in range(max(0,5-x),5):    #si proche du mur de gauche
                        for j in range(max(0,5-(salle.hauteur-y)),5):  #si proche du mur du bas
                            if salle.table[y+5-j][x-5+i]==0:   #la case est libre
                                self.positionx=x-5+i
                                self.positiony=y+5-j
                                return (True)
                elif (x<objectif.position[0][0]) and (y<objectif.position[0][1]):   #le personnage doit aller en bas à droite
                    for i in range(max(0,5-(salle.longueur-x)),5):    #si proche du mur de gauche
                        for j in range(max(0,5-(salle.hauteur-y)),5):  #si proche du mur du bas
                            if salle.table[y+5-j][x+5-i]==0:   #la case est libre
                                self.positionx=x+5-i
                                self.positiony=y+5-j
                                return (True)
        else:
            for i in range(salle.longueur):   #on recupere les coordonnées de l'objectif qui est une personne
                for j in range(salle.hauteur):
                    if salle.table[j,i]:
                        if salle.table[j,i][0]==objectif.numero:
                            x,y=i,j
            if salle.table[max(0,y-1),max(0,x-1)]==0:
                self.positionx=max(0,x-1)
                self.positiony=max(0,y-1)
            elif salle.table[y,max(0,x-1)]==0:
                self.positionx=max(0,x-1)
                self.positiony=y
            elif salle.table[min(y+1,salle.hauteur),max(0,x-1)]==0:
                self.positionx=max(0,x-1)
                self.positiony=min(y+1,salle.hauteur)
            elif salle.table[min(y+1,salle.hauteur),x]==0:
                self.positionx=x
                self.positiony=min(y+1,salle.hauteur)
            elif salle.table[max(0,y-1),x]==0:
                self.positionx=x
                self.positiony=max(0,y-1)
            elif salle.table[max(0,y-1),min(x+1,salle.longueur)]==0:
                self.positionx=min(x+1,salle.longueur)
                self.positiony=max(0,y-1)
            elif salle.table[y,min(x+1,salle.longueur)]==0:
                self.positionx=min(x+1,salle.longueur)
                self.positiony=y
            elif salle.table[min(y+1,salle.hauteur),min(x+1,salle.longueur)]==0:
                self.positionx=min(x+1,salle.longueur)
                self.positiony=min(y+1,salle.hauteur)
            
            
        
    def manger(self,buffet,localisation): # uniquement au buffet
        '''Le personnage mange '''
        self.faim -=4
        self.soif+=1
        buffet.stock -= 1
        if buffet.stock==0:
            buffet.fermeture()
            localisation.actualisationstand(buffet)
        self.vessie+=2
        self.fatigue -=4
        if self.fatigue<0:
            self.fatigue=0
        if self.faim<1:
            self.faim=0
            self.etat="inactif"
            buffet.departclient()
        if self.vessie>20:
            self.etat="vouloiruriner"
        elif rdm.random()>0.95:
            self.etat="inactif"
        
    def parler(self,personne):
        '''Le personnage parle : calcule en fonction des caractères des deux personnes la probabilité de continuer
        c'est calculé deux fois (une fois pour chaque personne) '''
        p=self.social*0.027777+personne.social*0.027777
        n=0
        if personne.cherche[0][0]<=self.taille and self.taille<=personne.cherche[0][1]:
            n+=0.05
        if self.cherche[0][0]<=personne.taille and personne.taille<=self.cherche[0][1]:
            n+=0.05
        if personne.cherche[1][0]<=self.humour and self.humour<=personne.cherche[1][1]:
            n+=0.05
        if self.cherche[1][0]<=personne.humour and personne.humour<=self.cherche[1][1]:
            n+=0.05
        if personne.cherche[2][0]<=self.beaute and self.beaute<=personne.cherche[2][1]:
            n+=0.05
        if self.cherche[2][0]<=personne.beaute and personne.beaute<=self.cherche[2][1]:
            n+=0.05
        if personne.cherche[3][0]<=self.sport and self.sport<=personne.cherche[3][1]:
            n+=0.05
        if self.cherche[3][0]<=personne.sport and personne.sport<=self.cherche[3][1]:
            n+=0.05
        if personne.cherche[4][0]<=self.culture and self.culture<=personne.cherche[4][1]:
            n+=0.05
        if self.cherche[4][0]<=personne.culture and personne.culture<=self.cherche[4][1]:
            n+=0.05
        p+=n
        t=rdm.random()
        if t>p:
            self.etat="inactif"
            personne.etat="inactif"
            self.parle1=0
            personne.parle1=0
            self.parle2=0
            personne.parle2=0
            self.parle3=0
            personne.parle3=0
        else:
            self.parle3+=1/2
            personne.parle3+=1/2
        
    def danser(self): # uniquement sur la piste
        '''Le personnage danse '''
        self.fatigue +=1 
        self.faim +=1
        self.soif+=1
        if self.fatigue>25:
            self.etat="repos"
        elif rdm.random()>0.95:
            self.etat="inactif"
        
    def uriner(self): # uniquement aux toilettes
        '''Le personnage va aux toilettes'''
        self.vessie = 0
        self.etat="inactif"
        
        
    def boire(self,bar,localisation): # uniquement au bar
        '''Le personnage bois de l'alcool'''
        self.soif -=5
        self.argent -=1
        bar.stock -=1
        if bar.stock==0:
            bar.fermeture()
            localisation.actualisationstand(bar)
        self.vessie+=3
        self.ebriete +=2
        self.fatigue -=3
        if self.fatigue<0:
            self.fatigue=0
        if self.soif<10:
            self.etat="inactif"
            bar.departclient()
        if self.vessie>20:
            self.etat="vouloiruriner"
        elif rdm.random()>0.95:
            self.etat="inactif"
            
        
    def partir(self,sortie,listepersonnages,salle):
        '''se dirige vers la sortie puis s'efface'''
        if self.isin(sortie):
            listepersonnages.delete(self.numero)
            salle.actualisation(listepersonnages)
        else:
            self.marcher(sortie,salle)
        
        

    
    def isin(self,lieu):
        x=self.positionx
        y=self.positiony
        if (x>=lieu.position[0][0]) and (x<=lieu.position[1][0]):
            if (y>=lieu.position[0][1]) and (y<=lieu.position[1][1]):
                return True
        return False

    
    def isaround(self,personne,salle):
        x=self.positionx
        y=self.positiony
        if salle.table[max(0,y-1),max(0,x-1)]:
                if salle.table[max(0,y-1),max(0,x-1)][0]==personne:
                    return True
        elif salle.table[y,max(0,x-1)]:
                if salle.table[y,max(0,x-1)][0]==personne:
                    return True
        elif salle.table[min(y+1,salle.hauteur),max(0,x-1)]:
                if salle.table[min(y+1,salle.hauteur),max(0,x-1)][0]==personne:
                    return True
        elif salle.table[min(y+1,salle.hauteur),x]:
                if salle.table[min(y+1,salle.hauteur),x][0]==personne:
                    return True
        elif salle.table[max(0,y-1),x]:
                if salle.table[max(0,y-1),x][0]==personne:
                    return True
        elif salle.table[max(0,y-1),min(x+1,salle.longueur)]:
                if salle.table[max(0,y-1),min(x+1,salle.longueur)][0]==personne:
                    return True
        elif salle.table[y,min(x+1,salle.longueur)]:
                if salle.table[y,min(x+1,salle.longueur)][0]==personne:
                    return True
        elif salle.table[min(y+1,salle.hauteur),min(x+1,salle.longueur)]:
                if salle.table[min(y+1,salle.hauteur),min(x+1,salle.longueur)][0]==personne:
                    return True
        return False
    
    
    def chercher(self,salle):
        a=[]
        x=self.positionx
        y=self.positiony
        for i in range(max(0,5-x),5):    #si proche du mur de gauche
            for j in range(max(0,5-y),5):
                if salle.table[max(0,y-i),max(0,x-j)]:    #haut à gauche
                    a+=[salle.table[max(0,y-i),max(0,x-j)]]
        for i in range(max(0,5-(salle.longueur-x)),5):    #si proche du mur de droite
            for j in range(max(0,5-y),5):            
                if salle.table[max(0,y-i),min(salle.longueur,x+j)]:    #haut à droite
                    a+=[salle.table[max(0,y-i),min(salle.longueur,x+j)]]
        for i in range(max(0,5-(salle.longueur-x)),5):    #si proche du mur de gauche
            for j in range(max(0,5-(salle.hauteur-y)),5):            
                if salle.table[min(salle.hauteur,y+i),min(salle.longueur,x+j)]:    #bas à droite
                    a+=[salle.table[min(salle.hauteur,y+i),min(salle.longueur,x+j)]]
        for i in range(max(0,5-x),5):    #si proche du mur de gauche
            for j in range(max(0,5-(salle.hauteur-y)),5):            
                if salle.table[min(salle.hauteur,y+i),max(0,x-j)]:     #bas à gauche
                    a+=[salle.table[min(salle.hauteur,y+i),max(0,x-j)]]
        b=[]
        if a:
            for i in a:
                if i[2]==3-self.sexe:   #on récupère les personnes de sexe opposés
                    b+=[i]
        c=[]
        if b:
            c=sorted(b,key=lambda colonnes:colonnes[1],reverse=True)
            n=rdm.randint(0,min(5,len(c))-1)
        if c:
            print(c[n][0])
            return c[n][0]
        return False
     
        
class CustomList:

    def __init__(self):
        # ghost node
        self.first = personnage(None,0,0,((0,0),(0,0),(0,0),(0,0),(0,0)),0,0,0,0,0,0,0,0)

    def append(self, numero,typer,depart,cherche,taille,humour,beaute,sportivite,culture,sociabilite,sexe,argent,position=(0,0),ebriete=0,fatigue=0,vessie=0,faim=5,soif=5):
        self.insert(float("inf"),numero,typer,depart,cherche,taille,humour,beaute,sportivite,culture,sociabilite,sexe,argent,position=(0,0),ebriete=0,fatigue=0,vessie=0,faim=5,soif=5)

    

    def insert(self, idx,numero,typer,depart,cherche,taille,humour,beaute,sportivite,culture,sociabilite,sexe,argent,position=(0,0),ebriete=0,fatigue=0,vessie=0,faim=5,soif=5):
        """Encapsulate data into a Node and append it to the list"""
        if idx < 0:
            print("Can not insert at negative index")
            return False
        new_node = personnage(numero,typer,depart,cherche,taille,humour,beaute,sportivite,culture,sociabilite,sexe,argent,position=(0,0),ebriete=0,fatigue=0,vessie=0,faim=5,soif=5)
        cur_node = self.first
        node_idx = 0
        while cur_node.next is not None and node_idx != idx:
            cur_node = cur_node.next
            node_idx += 1
        tmp = cur_node.next
        new_node.next = tmp
        cur_node.next = new_node
        return True


    def search(self, numero):
        cur_node = self.first
        node_idx = 0
        while cur_node.next is not None:
            if numero == cur_node.next.numero:
                return node_idx
            node_idx += 1
            cur_node = cur_node.next
        return None

    def delete(self, numero):
        cur_node = self.first
        prev_node = None

        while cur_node.next is not None:
            if numero == cur_node.next.numero:
                cur_node.next = cur_node.next.next
                return True
            prev_node = cur_node
            cur_node = cur_node.next
        if numero == cur_node.numero:
            prev_node.set_next(None)
            return True
        return False
    
    def __str__(self):
        n = self.first
        content_str = "The nodes in this list are: "
        while n is not None:
            content_str += str(n)
            n = n.next
        return content_str

if __name__ == "__main__":
    listepersonnages = CustomList()
    listepersonnages.append(1,"dragueur",10,((0,9),(0,9),(0,9),(0,9),(0,9)),5,5,5,5,5,5,1,(0,0),5)
    listepersonnages.append(2,"dragueur",10,((0,9),(0,9),(0,9),(0,9),(0,9)),5,5,5,5,5,5,2,(0,0),5)
    print(listepersonnages)