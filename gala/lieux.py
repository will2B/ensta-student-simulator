# -*- coding: utf-8 -*-
"""
Created on Wed Mar 23 10:26:24 2016

@author: Benjamin
"""

#==============================================================================
#Lieux
#==============================================================================

import numpy as np

class Stand():
    '''classe qui sert à définir tout les lieux de la "scène" avec des paramètres de capacités,
    fermeture, ouverture (stand photo,toilettes), 
    les personnages n'y sont pas représentés'''

    def __init__(self,nom,position,capacite,occupants=0,etat=True):
        self.position=position #Listes des coordonnées des coins, (hautgauche,basdroit) car rectangulaire
        self.numero=nom  #nom du lieux, nommé numero pour la fonction marcher(objectif)
        self.capacite = capacite #int
        self.occupants = occupants #nombre de personnes dans le lieu 
        self.etat = etat #bool

    def ouverture(self):
        '''ca ouvre'''
        self.etat = True
    def fermeture(self):
        '''ca ferme, dommage'''
        self.etat = False
    def arriveeclient(self):
        '''arrivée d'un client'''
        self.occupants +=1
    
    def departclient(self):
        self.occupants -=1
    
    
        

class Consommation(Stand):
    '''sous classe pour définir les stands avec un stock'''

    def __init__(self,nom,position,capacité,stock,occupants=0,état=False):
        super().__init__(nom,position,capacité,occupants,état)
        self.stock = stock #int

#    def vendre(self):
#        '''un client achète un article'''
#        self.stock -= 1
    def ravitaillement(self,quantité):
        '''le stand est réaprovisionné'''
        self.stock = self.stock + quantité
        self.ouverture()
#    def ferme(self):
#        '''y a plus de stock'''
#        if (self.stock==0):
#            self.fermeture()


class Salle():
    '''classe qui contient les informations sur les emplacements des personnes présentes ainsi que certains critères (beauté,numéro,sexe)'''
    
    def __init__(self,longueur,hauteur):
        self.longueur=longueur
        self.hauteur=hauteur
        self.table=np.zeros((self.hauteur+1,self.longueur+1),dtype=list)
        
    
    def actualisation(self,listepersonnages):
        p=listepersonnages.first
        self.table=np.zeros((self.hauteur+1,self.longueur+1),dtype=list)  #on reinitialise la table pour eviter les doublons
        while p.next is not None:
            p=p.next
            x=p.positionx
            y=p.positiony
            self.table[y,x]=[p.numero,p.beaute,p.sexe]        #on affecte les informations sur la personne présente sur chaque case occupée
    
    
    def __str__(self):
        a=''
        for i in range(self.hauteur):
            for j in range(self.longueur):
                a+=str(self.table[i,j]) + ' '
            a+='\n'
        return a
                
    

class Localisation(Salle):
    '''Sous-classe de Salle qui fonctionne de la même manière mais qui ne stocke que les informations sur les Stands (ouvert, fermé)'''
    
    def __init__(self,longueur,hauteur):
        super().__init__(longueur,hauteur)
        
    
    def actualisationstand(self,stand):
        for i in range(stand.position[0][0],stand.position[1][0]+1):
            for j in range(stand.position[0][1],stand.position[1][1]+1):
                x=2
                if stand.etat:
                    x=1
                self.table[j,i]=[stand.numero,x]


if __name__=="__main__":
    toilettes=Stand("toilettes",((0,5),(2,9)),10)
    bar=Consommation("bar",((10,10),(15,20)),20,50)
    buffet=Consommation("buffet",((20,10),(25,20)),20,50)    
    localisation=Localisation(30,30)
    print(localisation)
    localisation.actualisationstand(toilettes)
    print(localisation)
    localisation.actualisationstand(buffet)
    print(localisation)
    localisation.actualisationstand(bar)
    print(localisation)