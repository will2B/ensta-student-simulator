"""
    coding  : utf-8
    author  : William Blachère
    created : 05/15/17
"""


## Importations ##

import re


## Liste 'dynamique' des personnes ##

#require 'pp'
#require 'strscan'
#require 'benchmark'

class Token

    def __init__ (self, tab):
        self.kind;
        self.val;
        self.pos = tab;


    def is_a (kind):
        case kind
            when Symbol
                return self.kind==kind
            when Array
                for sym in kind
                return true if self.kind==sym
            return false
            else
            raise "wrong type during lookahead"

class Lexer

    # I like explicit things :
    FOR               = /\Afor$/
    IF                = /\Aif$/
    ELSE              = /\Aelse$/
    BREAK             = /\Abreak$/
    WHILE             = /\Awhile$/
    RETURN            = /\Areturn$/
    STRUCT            = /\Astruct$/
    TYPEDEF           = /\Atypedef$/
    SIZEOF            = /\Asizeof/
    SWITCH            = /\Aswitch/
    CASE              = /\Acase/
    DEFAULT           = /\Adefault/
    DO                = /\Ado/
    VOID              = /\Avoid/
    GOTO              = /\Agoto/

    #................................................
    NEWLINE            = /[\n]/
    SPACE              = /[ \t]+/

    #.............punctuation........................
    LPAREN             = /\A\(/
    RPAREN             = /\A\)/
    LBRACE             = /\A\{/
    RBRACE             = /\A\}/
    LBRACK             = /\A\[/
    RBRACK             = /\A\]/
    SEMICOLON          = /\A;/
    COLON              = /\A:/
    COMMA              = /\A\,/
    LCOMMENT           = /\A\/\*/
    RCOMMENT           = /\A\*\//
    COMMENT            = /\A\/\/(^(\*\/).*)/

    # .............literals.........................
    IDENTIFIER         = /\A[a-zA-Z]\w*/i
    INTEGER_LITERAL    = /\A\d+/
    STRING_LITERAL     = /"[^"]*"/ 
    CHAR_LITERAL       = /\A\'(.*)\'/
    FLOAT_LITERAL      = /\d*(\.\d+)(E([+-]?)\d+)?/
    BOOLEAN_LITERAL       = /\A(true|false)/
    """

    attr_accessor :suppress_comment

    def initialize str=''
        init(str)
        @suppress_space=true
        @suppress_comment=false
    end

    def init str
        @ss=StringScanner.new(str)
        @line=0
    end

    def tokenize str
        @tokens=[]
        init(str)
        until @ss.eos?
            @tokens << next_token()
    end
        return @tokens[0..-2]
    end

    #next token can detect spaces length
    def next_token

        if @ss.bol?
            @line+=1
            @old_pos=@ss.pos
        end

        position=[@line,@ss.pos-@old_pos+1]

        return :eos if @ss.eos?

    case
    when text = @ss.scan(NEWLINE)
    next_token()
    when text = @ss.scan(SPACE)
    next_token()

    # ............... punctuation
    when text = @ss.scan(DOT)
    return Token.new [:dot,text,position]
    when text = @ss.scan(LPAREN)
    return Token.new [:lparen,text,position]
    when text = @ss.scan(RPAREN)
    return Token.new [:rparen,text,position]
    when text = @ss.scan(LBRACE)
    return Token.new [:lbrace,text,position]
    when text = @ss.scan(RBRACE)
    return Token.new [:rbrace,text,position]
    when text = @ss.scan(LBRACK)
    return Token.new [:lbrack,text,position]
    when text = @ss.scan(RBRACK)
    return Token.new [:rbrack,text,position]
    when text = @ss.scan(EQ)
    return Token.new [:eq,text,position]
    when text = @ss.scan(ASSIGN)
    return Token.new [:assign,text,position]
    when text = @ss.scan(SEMICOLON)
    return Token.new [:semicolon,text,position]
    when text = @ss.scan(COLON)
    return Token.new [:colon,text,position]
    when text = @ss.scan(COMMA)
    return Token.new [:comma,text,position]
    when text = @ss.scan(LCOMMENT)
    return Token.new [:lcomment,text,position]
    when text = @ss.scan(RCOMMENT)
    return Token.new [:rcomment,text,position]
    when text = @ss.scan(COMMENT)
    return Token.new [:comment,text,position]
    # placed here for precedence reason wrt ADD SUB
    when text= @ss.scan(FLOAT_LITERAL)
    return Token.new [:float_literal,text,position]
    when text= @ss.scan(INTEGER_LITERAL)
    return Token.new [:integer_literal,text,position]
    when text = @ss.scan(BOOLEAN_LITERAL)
    return Token.new [:boolean_literal,text,position]
    # ............... operators
    when text = @ss.scan(ADDADD)
    return Token.new [:addadd,text,position]
    when text = @ss.scan(ADDEQ)
    return Token.new [:addeq,text,position]
    when text = @ss.scan(ADD)
    return Token.new [:add,text,position]
    when text = @ss.scan(SUBSUB)
    return Token.new [:subsub,text,position]
    when text = @ss.scan(SUBEQ)
    return Token.new [:subeq,text,position]
    when text = @ss.scan(SUB)
    return Token.new [:sub,text,position]
    when text = @ss.scan(MUL)
    return Token.new [:mul,text,position]
    when text = @ss.scan(DIV)
    return Token.new [:div,text,position]
    when text = @ss.scan(MOD)
    return Token.new [:mod,text,position]
    when text = @ss.scan(NOT)
    return Token.new [:not,text,position]
    when text = @ss.scan(NEQ)
    return Token.new [:neq,text,position]
    when text = @ss.scan(DBAR)
    return Token.new [:dbar,text,position]
    when text = @ss.scan(LTE)
    return Token.new [:lte,text,position]
    when text = @ss.scan(LT)
    return Token.new [:lt,text,position]
    when text = @ss.scan(GTE)
    return Token.new [:gte,text,position]
    when text = @ss.scan(GT)
    return Token.new [:gt,text,position]

    when text = @ss.scan(DAMPERSAND)
    return Token.new [:dampersand,text,position]
    when text = @ss.scan(AMPERSAND)
    return Token.new [:ampersand,text,position]
    when text = @ss.scan(SHARP)
    return Token.new [:sharp,text,position]
    # ............... literals .................................

    when text = @ss.scan(STRING_LITERAL)
    return Token.new [:string_lit,text,position]
    when text = @ss.scan(CHAR_LITERAL)
    return Token.new [:char_lit,text,position]
    #........... identifiers / keywords discrimination..........
    when text = @ss.scan(IDENTIFIER)
    case
    when value = text.match(INT)
    return Token.new [:int,text,position]
    when value = text.match(CHAR)
    return Token.new [:char,text,position]
    when value = text.match(SHORT)
    return Token.new [:short,text,position]
    when value = text.match(LONG)
    return Token.new [:long,text,position]
    when value = text.match(FLOAT)
    return Token.new [:float,text,position]
    when value = text.match(DOUBLE)
    return Token.new [:double,text,position]
    when value = text.match(SIGNED)
    return Token.new [:signed,text,position]
    when value = text.match(UNSIGNED)
    return Token.new [:unsigned,text,position]
    when value = text.match(VOID)
    return Token.new [:void,text,position]
    when value = text.match(FOR)
    return Token.new [:for,text,position]
    when value = text.match(IF)
    return Token.new [:if,text,position]
    when value = text.match(ELSE)
    return Token.new [:else,text,position]
    when value = text.match(BREAK)
    return Token.new [:break,text,position]
    when value = text.match(WHILE)
    return Token.new [:while,text,position]
    when value = text.match(RETURN)
    return Token.new [:return,text,position]
    when value = text.match(STRUCT)
    return Token.new [:struct,text,position]
    when value = text.match(TYPEDEF)
    return Token.new [:typedef,text,position]
    when value = text.match(SIZEOF)
    return Token.new [:sizeof,text,position]
    when value = text.match(SWITCH)
    return Token.new [:switch,text,position]
    when value = text.match(CASE)
    return Token.new [:case,text,position]
    when value = text.match(DEFAULT)
    return Token.new [:default,text,position]
    when value = text.match(DO)
    return Token.new [:do,text,position]
    when value = text.match(GOTO)
    return Token.new [:goto,text,position]
    else
    return Token.new [:ident,text,position]
    end

    else
    x = @ss.getch
    return Token.new [x, x,position]
    end
    end

    end

if $PROGRAM_NAME == __FILE__
  str=IO.read(ARGV[0])
  #str.downcase!
  puts str
  t1 = Time.now
  lexer=CLexer.new
  tokens=lexer.tokenize(str)
  t2 = Time.now
  pp tokens
  puts "number of tokens : #{tokens.size}"
  puts "tokenized in     : #{t2-t1} s"
end
