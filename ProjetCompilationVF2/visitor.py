from ast import *

class Visitor:

    def __init__(self, pp):
        self.pp = pp
        self.indentation = 0

    # definition des indentations
    def indent(self):
        self.indentation += 2

    def desindent(self):
        self.indentation -= 2

    # lancement du visiteur
    def doIt(self, ast):
        print "----------------------VISITOR----------------------"
        self.visitRacine(ast, None)

    # fonction pour afficher dans le terminal
    def say(self, txt):
        print " " * self.indentation + txt
    
    def visitIdentifier(self, id, args=None):
        self.say("visitIdentifier")
        self.indent()
        self.say(id.name)
        self.desindent()
        self.pp.write('<span class="autre">' + str(id.name) + '<span>ident</span></span>')
    
    def visitOperateur(self, op, args=None):
        self.say("visitOperateur")
        self.indent()
        self.say(op.val)
        self.desindent()
        self.pp.write(' <em>' + str(op.val) + '<span>op</span></em> ')

    # premiere methode appelee par doIt qui declenche la visite des classes filles de lAST
    def visitRacine(self, root, args=None):
        self.say("visitRacine")
        self.indent()
        if (root.includes or root.raccourcis):
            self.pp.write('<p>\n')
        for inc in root.includes:
            inc.accept(self, args)
        for rac in root.raccourcis:
            rac.accept(self, args)
        if (root.includes or root.raccourcis):
            self.pp.write('</p>\n')
        if root.main:
            self.pp.write('<p>\n')
            root.main.accept(self, args)
            self.pp.write('</p>\n')
        self.desindent()

    def visitInclude(self, inc, args=None):
        self.say("visitInclude")
        self.indent()
        self.pp.write('<em class="info">#include<span>keyword</span></em> <span class="argument">&lt;')
        inc.val.accept(self, args)
        self.desindent()
        self.pp.write('&gt;</span><br />')
    
    def visitRaccourci(self, rac, args=None):
        self.say("visitRaccourci")
        self.indent()
        self.pp.write('<em>using<span>keyword</span> namespace<span>keyword</span></em> ')
        rac.val.accept(self, args)
        self.desindent()
        self.pp.write(';<br />')

    def visitMain(self, main, args=None):
        self.say("visitMain")
        self.indent()
        main.type1.accept(self, args)
        self.pp.write(' <span class="autre">main<span>keyword</span></span>(')
        i = 0
        while i != len(main.ident):
            main.type2[i].accept(self, args)
            main.ident[i].accept(self, args)
            i += 1
            if i != len(main.ident):
                self.pp.write(', ')
        self.pp.write(')<br />\n')
        self.pp.write('{<div class="indent">\n')
        if main.statements:
            for stat in main.statements:
                self.pp.write('<p>')
                stat.accept(self, args)
                self.pp.write('</p>')
        self.desindent()
        self.pp.write('</div>\n}\n')

    def visitType(self, type, args=None):
        self.say("visitType")
        self.indent()
        self.say(type.val)
        self.desindent()
        self.pp.write('<span class="type">' + type.val + '<span>type</span></span> ')

    def visitNumber(self, num, args=None):
        self.say("visitNumber")
        self.indent()
        num.type.accept(self, args)
        self.desindent()

    def visitDouble(self, d, args=None):
        self.say("visitDouble")
        self.indent()
        self.say(d.val)
        self.desindent()
        self.pp.write('<span class="autre">' + str(d.val) + '<span>double</span></span>')

    def visitInt(self, int, args=None):
        self.say("visitInt")
        self.indent()
        self.say(int.val)
        self.desindent()
        self.pp.write('<span class="autre">' + str(int.val) + '<span>int</span></span>')

    def visitTerm(self, term, args=None):
        self.say("visitTerm")
        self.indent()
        if term.minus:
            term.minus.accept(self, args)
        term.val.accept(self, args)
        self.desindent()

    def visitQuote(self, q, args=None):
        self.say("visitQuote")
        self.indent()
        self.say(q.val)
        self.desindent()
        self.pp.write(' <span class="string">"' + q.val + ' "<span>quote</span></span>')

    def visitDeclareMethode(self, dec, args=None):
        self.say("visitDeclareMethode")
        self.indent()
        dec.type.accept(self, args)
        dec.name.accept(self, args)
        i = 0
        while i != len(dec.fils):
            dec.fils[i][0].accept(self, args)
            dec.fils[i][1].accept(self, args)
            i += 1
        self.desindent()

    def visitMethode(self, met, args=None):
        self.say("visitMethode")
        self.indent()
        met.declaration.accept(self, args)
        if met.statements:
            for m in met.statements:
                m.accept(self, args)
        self.desindent()

    def visitFonctionMath(self, fm, args=None):
        self.say("visitFonctionMath")
        self.indent()
        self.say(fm.type1)
        self.pp.write('<span class="autre">' + str(fm.type1) + '<span>keyword</span></span>(')
        if fm.args:
            fm.args[0].accept(self, args)
            for f in fm.args[1:]:
                self.pp.write(', ')
                f.accept(self, args)
        self.desindent()
        self.pp.write(')')

    def visitFonctionPrint(self, fp, args=None):
        self.say("visitFonctionPrint")
        self.indent()
        self.say(fp.name)
        if fp.name == 'cout':
            chevrons = "<<"
            self.pp.write('<span class="autre">' + str(fp.name) + '<span>keyword</span></span> <em>' + chevrons + '</em> ')
        else:
            chevrons = ">>"
            self.pp.write('<span class="autre">' + str(fp.name) + '<span>keyword</span></span> <em>' + chevrons + '</em> ')
        if fp.args:
            for f in fp.args:
                f.accept(self, args)
                if fp.name == 'cout':
                    self.pp.write(' <em>' + chevrons + '</em> ')
        self.desindent()
        if fp.name == 'cout':
            self.pp.write(' <span class="autre">endl<span>keyword</span></span>')

    def visitSimpleExpression(self, se, args=None):
        self.say("visitSimpleExpression")
        self.indent()
        se.val.accept(self, args)
        self.desindent()

    def visitExpression(self, exp, args=None):
        self.say("visitExpression")
        self.indent()
        exp.simpleExpr[0].accept(self, args)
        i = 0
        while i != len(exp.operateurs):
            exp.operateurs[i].accept(self, args)
            exp.simpleExpr[i+1].accept(self, args)
            i += 1
        self.desindent()

    def visitExpressionC(self, expc, args=None):
        self.say("visitExpressionC")
        self.indent()
        #self.pp.write('<p>')
        expc.expr.accept(self, args)
        self.desindent()
        self.pp.write(';\n')

    def visitAssignment(self, assig, args=None):
        self.say("visitAssignment")
        self.indent()
        assig.type1.accept(self, args)
        assig.args[0].accept(self, args)
        if assig.operateurs:
            assig.operateurs[0].accept(self, args)
            assig.args[1].accept(self, args)
        else:
            for a in assig.args[1:]:
                self.pp.write(', ')
                a.accept(self, args)
        self.desindent()
        self.pp.write(';\n')

    def visitIfStatement(self, ifs, args=None):
        self.say("visitIfStatement")
        self.indent()
        self.pp.write('<em>if<span>keyword</span></em> (')
        ifs.condition.accept(self, args)
        self.pp.write(') {\n<div class="indent">')
        for s in ifs.statements:
            s.accept(self, args)
            self.pp.write('<br />\n')
        self.pp.write('</div>}<br />\n')
        i = 0
        while i != len(ifs.conditionElseIf):
            self.pp.write('<em>else if<span>keyword</span></em> (')
            ifs.conditionElseIf[i].accept(self, args)
            self.pp.write(') {\n<div class="indent">')
            for st in ifs.statementsElseIf[i]:
                st.accept(self, args)
                self.pp.write('<br />\n')
            self.pp.write('</div>}<br />\n')
            i += 1
        if ifs.statementsElse:
            self.pp.write('<em>else<span>keyword</span></em> {\n<div class="indent">')
            for e in ifs.statementsElse:
                e.accept(self, args)
                self.pp.write('<br />\n')
            self.pp.write('</div>}<br />\n')
        self.desindent()

    def visitForStatement(self, fs, args=None):
        self.say("visitForStatement")
        self.indent()
        self.pp.write('<em>for<span>keyword</span></em> (')
        fs.stmt.accept(self, args)
        fs.exprC.accept(self, args)
        fs.expr.accept(self, args)
        self.pp.write(') {\n<div class="indent">')
        if fs.statements:
            for f in fs.statements:
                f.accept(self, args)
                self.pp.write('<br />\n')
        self.desindent()
        self.pp.write('</div>}<br />\n')

    def visitWhileStatement(self, ws, args=None):
        self.say("visitWhileStatement")
        self.indent()
        self.pp.write('<em>while<span>keyword</span></em> (')
        ws.expr.accept(self, args)
        self.pp.write(') {\n<div class="indent">')
        if ws.statements:
            for w in ws.statements:
                w.accept(self, args)
                self.pp.write('<br />\n')
        self.desindent()
        self.pp.write('</div>}<br />\n')

    def visitReturn(self, ret, args=None):
        self.say("visitReturn")
        self.indent()
        self.pp.write('<p><em>return<span>keyword</span></em> ')
        ret.expr.accept(self, args)
        self.desindent()
        self.pp.write('</p>')