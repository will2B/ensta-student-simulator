from generic_lexer import Lexer
from ast import *
import sys

class Parser:

    def __init__(self): #Notre methode constructeur
        self.lexer = Lexer()

    #fonction get permettant de recuperer le lexer
    def get_lexer(self):
        return self.lexer

    def set_stream(self, val):
        self.lexer = val
    
    #fonction permettant de tester le parsing des differents elements de la grammaire
    #cette fonction doit etre renommee en parse et le compilateur lance avec test3.cpp
    def parsetest3(self, filename):
        file = open(filename)
        str = file.readlines()
        self.lexer.tokenize(str)
        print "---------------------PARSER----------------"
        #test include
        include = self.parseInclude()
        print include
        #test raccourci
        raccourci = self.parseRaccourci()
        print raccourci
        #test type
        type1 = self.parseType()
        print type1
        #test term ident
        term = self.parseTerm()
        print term
        #test term number
        term1 = self.parseTerm()
        print term1
        #test quote
        quote = self.parseQuote()
        print quote
        #test declareMethode
        declaration = self.parseDeclareMethode()
        print declaration
        #test fonctionMath
        fonction = self.parseFonctionMath()
        print fonction
        #test fonction
        fonction = self.parseFonction()
        print fonction
        #test simpleExpression
        simpleExpr = self.parseSimpleExpression()
        print simpleExpr
        #test fonctionPrint
        fonprint = self.parseFonctionPrint()
        print fonprint
        #test Expression
        expr = self.parseExpression()
        print expr
        #test ExpressionC
        exprC = self.parseExpressionC()
        print exprC
        #test Assignment
        assig = self.parseAssignment()
        print assig
        #test if
        If = self.parseIfStatement()
        print If
        #test for
        For = self.parseForStatement()
        print For
        #test while
        While = self.parseWhileStatement()
        print While
        #test return
        ret = self.parseReturn()
        print ret
        #test term
        number = self.parseNumber()
        print number
        #test main
        main = self.parseMain()
        print main
        

    def parse(self, filename):
        file = open(filename)
        str = file.readlines()
        self.lexer.tokenize(str)
        print "----------------------PARSER--------------------------"
        #print self.get_lexer().get_stream()
        ast = Racine()
        while (self.lexer.get_stream() != [] and self.showNext().kind == 'include'):
            ast.includes.append(self.parseInclude())
        while (self.lexer.get_stream() != [] and self.showNext().kind == 'using'):
            ast.raccourcis.append(self.parseRaccourci())
        while(self.lexer.get_stream() != []):
            #self.parseMethode()
            ast.main = self.parseMain()
        print ast
        return ast


    #verfie que le prochain token est du type attendu et retourne ce token
    def expect(self, token_kind):
        next_tok=self.lexer.get_next()
        if next_tok.kind !=token_kind:
            print "\nparsing error on line {0} pos {1}".format(next_tok.pos[0],next_tok.pos[1])
            print "expecting "+token_kind+". Got "+ next_tok.kind
            sys.exit(1)
        return next_tok
    
    #retourne le prochain token
    def showNext(self):
        return self.lexer.show_next()
    
    #consomme un token et le retourne
    def acceptIt(self):
        token = self.lexer.get_next()
        return token


#Merci de vous referer a grammaire.txt pour voir l'EBNF qui a permis de realiser les fonctions parse suivantes

    def parseInclude(self):
        print "parseInclude"
        self.expect('include')
        self.expect('inferior')
        ident = self.expect('ident')
        include = Include(Identifier(ident.value))
        self.expect('superior')
        return include
    
    def parseRaccourci(self):
        print "parseRaccourci"
        self.expect('using')
        self.expect('namespace')
        ident = self.expect('ident')
        raccourci = Raccourci(Identifier(ident.value))
        self.expect('semicolon')
        return raccourci

    def parseMain(self):
        print "parseMain"
        main = Main()
        main.type1 = self.parseType()
        self.expect('main')
        self.expect('lparen')
        if (self.showNext().kind != 'rparen') :
            main.type2.append(self.parseType())
            main.ident.append(Identifier(self.expect('ident').value))
            while (self.showNext().kind == 'comma'):
                self.acceptIt()
                main.type2.append(self.parseType())
                main.ident.append(Identifier(self.expect('ident').value))
        self.expect('rparen')
        self.expect('laccolade')
        while(self.showNext().kind != 'raccolade') :
            main.statements.append(self.parseStatement())
        self.expect('raccolade')
        return main
            
    def parseNumber(self):
        print "parseNumber"
        number = Number()
        if (self.showNext().kind == 'integer'):
            number.type = Int(self.showNext().value)
            self.acceptIt()
        elif (self.showNext().kind == 'doubleNumber'):
            number.type = Double(self.showNext().value)
            self.acceptIt()
        else:
            print "expecting integer or double"
            sys.exit(1)
        return number
        
    def parseType(self):
        print "parseType"
        type1 = Type()
        if(self.showNext().kind == 'int'):
            type1.val = self.acceptIt().kind
        elif(self.showNext().kind == 'double'):
            type1.val = self.acceptIt().kind
        else:
            raise "parsing error for type"
        return type1
            
    def parseTerm(self):
        print "parseTerm"
        term = Term()
        if (self.showNext().kind == 'minus'):
        	term.minus = Operateur(self.acceptIt().value)
        if (self.showNext().kind == 'ident'):
             ident = self.acceptIt()
             identifier = Identifier(ident.value)
             term.val = identifier
        elif (self.showNext().kind in ['integer', 'doubleNumber']):
            term.val = self.parseNumber()
        else:
            print "\nligne ", self.showNext().pos[0], ": '"+self.showNext().value+"'.", "Expecting : identifier number or ident. "
            sys.exit(1)
        return term

    def parseQuote(self):
        print "parseQuote"
        self.expect('guillemet')
        string = ""
        while(self.showNext().kind != 'guillemet'):
            string = string + " " + self.acceptIt().value
        self.expect('guillemet')
        return Quote(string)

    def parseDeclareMethode(self):
        print "parseDeclareMethode"
        declaration = DeclareMethode()
        declaration.type = self.parseType()
        declaration.name = Identifier(self.expect('ident').value)
        self.expect('lparen')
        if (self.showNext().kind != 'rparen') :
            type1 = self.parseType()
            ident = Identifier(self.expect('ident').value)
            declaration.fils.append([type1,ident])
            while (self.showNext().kind == 'comma'):
                self.acceptIt()
                type1 = self.parseType()
                ident = Identifier(self.expect('ident').value)
                declaration.fils.append([type1,ident])
        self.expect('rparen')
        return declaration
        
    def parseMethode(self):
        print "parseMethode"
        methode = Methode()
        methode.declaration = self.parseDeclareMethode()
        self.expect('laccolade')
        while(self.showNext().kind != 'raccolade') :
            methode.statements.append(self.parseStatement())
        self.expect('raccolade')
        return methode

    def parseFonctionMath(self):
        print "parseFonctionMath"
        if (self.showNext().kind == 'cos'):
            fonction = FonctionMath(self.acceptIt().kind)
        elif (self.showNext().kind == 'sin'):
            fonction = FonctionMath(self.acceptIt().kind)
        elif (self.showNext().kind == 'pow'):
            fonction = FonctionMath(self.acceptIt().kind)
        elif (self.showNext().kind == 'sqrt'):
            fonction = FonctionMath(self.acceptIt().kind)
        else:
            print "parsin error for FonctionMath ligne : ", self.showNext().pos[0]
            sys.exit(1)
        return fonction

    def parseFonction(self):
        print "parseFonction"
        fonction = self.parseFonctionMath()
        self.expect('lparen')
        fonction.args.append(self.parseExpression())
        while(self.showNext().kind == 'comma'):
            self.acceptIt()
            fonction.args.append(self.parseExpression())
        self.expect('rparen')
        return fonction

    def parseFonctionPrint(self):
        print "parseFonctionPrint"
        fon = FonctionPrint(self.showNext().kind)
        if (self.showNext().kind == 'cout'):
            self.acceptIt()
            self.expect('inferior')
            self.expect('inferior')
            if (self.showNext().kind == 'guillemet'):
                fon.args.append(self.parseQuote())
            else:
                fon.args.append(self.parseSimpleExpression())
            self.expect('inferior')
            self.expect('inferior')
            while(self.showNext().kind != 'endl'):
                if (self.showNext().kind == 'guillemet'):
                    fon.args.append(self.parseQuote())
                else:
                    fon.args.append(self.parseSimpleExpression())
                self.expect('inferior')
                self.expect('inferior')
            self.expect('endl')
        elif (self.showNext().kind == 'cin'):
            self.acceptIt()
            self.expect('superior')
            self.expect('superior')
            fon.args.append(Identifier(self.expect('ident').value))
        else:
            raise "parsin error for FonctionPrint ligne : ", self.showNext().pos
        return fon

    def parseSimpleExpression(self):
        print "parseSimpleExpression"
        if (self.showNext().kind in ['cos', 'sin', 'pow', 'sqrt']):
            res = SimpleExpression(self.parseFonction())
        else:
            res = SimpleExpression(self.parseTerm())
        return res

    def parseExpression(self):
        print "parseExpression"
        expr=Expression()
        expr.simpleExpr.append(self.parseSimpleExpression())
        while (self.showNext().kind in ['doubleEqual', 'equal', 'plus', 'minus', 'inferior', 'superior', 'inferiorEqual', 'superiorEqual', 'divide', 'multiply']):
            expr.operateurs.append(Operateur(self.acceptIt().value))
            expr.simpleExpr.append(self.parseSimpleExpression())
        return expr
	        
    def parseExpressionC(self):
        print "parseExpressionC"
        exprC = ExpressionC()
        if (self.showNext().kind in ['cout','cin']):
            exprC.expr=self.parseFonctionPrint()
        else:
            exprC.expr=self.parseExpression()
        self.expect('semicolon')
        return exprC
        
    def parseAssignment(self):
        print "parseAssignment"
        assig = Assignment(self.parseType())
        assig.args.append(Identifier(self.expect('ident').value))
        if (self.showNext().kind == 'comma'):
            self.acceptIt()
            assig.args.append(Identifier(self.expect('ident').value))
            while(self.showNext().kind != 'semicolon'):
                self.expect('comma')
                assig.args.append(Identifier(self.expect('ident').value))
        else:
            assig.operateurs.append(Operateur(self.expect('equal').value))
            assig.args.append(self.parseExpression())
        self.expect('semicolon')
        return assig

    def parseIfStatement(self):
        print "parseIfStatement"
        #Parse condition if
        self.expect('if')
        self.expect('lparen')
        If = IfStatement(self.parseExpression())
        self.expect('rparen')
        #parse liste d'instructions
        if (self.showNext().kind == 'laccolade'):
            self.acceptIt()
            while (self.showNext().kind != 'raccolade'):
                If.statements.append(self.parseStatement())
            self.expect('raccolade')
        else:
             If.statements.append(self.parseStatement())
        #Parse else if s'il est present    
        while (self.showNext().kind == 'else if'):
            self.acceptIt()
            self.expect('lparen')
            If.conditionElseIf.append(self.parseExpression())
            self.expect('rparen')
            #parse liste d'instructions
            stmt=[]
            if (self.showNext().kind == 'laccolade'):
                self.acceptIt()
                while (self.showNext().kind != 'raccolade'):
                    stmt.append(self.parseStatement())
                self.expect('raccolade')
            else:
                stmt.append(self.parseStatement())
            If.statementsElseIf.append(stmt)
        #Parse else s'il est present
        if (self.showNext().kind == 'else'):
            self.acceptIt()
            #parse liste d'instructions
            if (self.showNext().kind == 'laccolade'):
                self.acceptIt()
                while (self.showNext().kind != 'raccolade'):
                    If.statementsElse.append(self.parseStatement())
                self.expect('raccolade')
            else:
                If.statementsElse.append(self.parseStatement())
        return If

	
                
    def parseForStatement(self):
        print "parseForStatement"
        #Parse for
        self.expect('for')
        self.expect('lparen')
        stmt = self.parseStatement()
        exprC = self.parseExpressionC()
        expr = self.parseExpression()
        For = ForStatement(stmt, exprC, expr)
        self.expect('rparen')
        #parse liste d'instructions
        if (self.showNext().kind == 'laccolade'):
            self.acceptIt()
            while (self.showNext().kind != 'raccolade'):
                For.statements.append(self.parseStatement())
            self.expect('raccolade')
        else:
            For.statements.append(self.parseStatement())
        return For
        

    def parseWhileStatement(self):
        print "parseWhileStatement"
        #Parse while
        self.expect('while')
        self.expect('lparen')
        While = WhileStatement(self.parseExpression())
        self.expect('rparen')
        #parse liste d'instructions
        if (self.showNext().kind == 'laccolade'):
            self.acceptIt()
            while (self.showNext().kind != 'raccolade'):
                While.statements.append(self.parseStatement())
            self.expect('raccolade')
        else:
            While.statements.append(self.parseStatement())
        return While
        

    def parseReturn(self):
        print "parseReturn"
        self.expect('return')
        ret = Return(self.parseExpressionC())
        return ret
        
    def parseStatement(self):
        print "parseStatement"
        if(self.showNext().kind == 'for'):
            stmt = self.parseForStatement()
        elif(self.showNext().kind == 'while'):
            stmt = self.parseWhileStatement()
        elif(self.showNext().kind == 'if'):
            stmt = self.parseIfStatement()
        elif(self.showNext().kind == 'return'):
            stmt = self.parseReturn()
        elif(self.showNext().kind in ['int', 'double']):
            stmt = self.parseAssignment()
        else:
            stmt = self.parseExpressionC()
        return stmt




if __name__ == '__main__':
    parser=Parser()
    parser.parse(sys.argv[1])


















