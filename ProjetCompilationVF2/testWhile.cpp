#include <iostream>

using namespace std;

int main()
{
    int nbEnfants = -1;
    
    while (nbEnfants < 0)
    {
        cout << "Combien d'enfants avez-vous ?" << endl;
        cin >> nbEnfants;
    }
    
    if (nbEnfants == 0)
        cout << "Vous en avez " << nbEnfants << " enfant !" << endl;
    else if (nbEnfants == 1)
        cout << "Vous en avez " << nbEnfants << " enfant !" << endl;
    else if (nbEnfants < 5)
        cout << "Vous en avez " << nbEnfants << " enfants !" << endl;
    else
        cout << nbEnfants << " enfants vraiment ? Vous en avez beaucoup !" << endl;
    
    return 0;
}
