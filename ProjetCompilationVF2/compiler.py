from ast import *
from parser import Parser
import sys
import os

class Compiler:

    def __init__(self):
        print "MINI C++ COMPILER"
        self.parser = Parser()

    def compile(self, filename,nameHtml):
        print "COMPILING " + filename
        ast = self.parser.parse(filename)
        self.simpleVisit(ast,nameHtml)

    def simpleVisit(self, ast,nameHtml):
        # on supprimer le fichier du pretty printer sil existe deja
        if os.path.exists(nameHtml):
            os.remove(nameHtml)
        # on cree le pretty printer en mode ajout
        pp = open(nameHtml, 'a')
        # debut pretty printer
        pp.write('<!DOCTYPE html>\n')
        pp.write('<html>\n')
        pp.write('<head>\n')
        pp.write('<meta charset="utf-8" />\n')
        pp.write('<link rel="stylesheet" href="style.css" />\n')
        pp.write('<title>' + nameHtml + '</title>\n')
        pp.write('</head>\n')
        pp.write('<body>\n')
        # creation du visiteur
        visitor = Visitor(pp)
        visitor.doIt(ast)
        # fin pretty printer
        pp.write('</body>\n')
        pp.write('</html>')
        # on ferme le fichier
        pp.close()

if __name__ == '__main__':
    compiler = Compiler()
    
    # on traite les erreurs sil manque des arguments
    if (len(sys.argv)==1):
        print "C++ file needed !"
        sys.exit(1)
    elif (len(sys.argv)==2):
        print "html name needed !"
        sys.exit(1)
    else:
        compiler.compile(sys.argv[1],sys.argv[2])