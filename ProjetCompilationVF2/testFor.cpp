#include <iostream>
#include <cmath>

using namespace std;

int main()
{
    
    cout << "Je sais compter jusqu'a 10 !" << endl;
    
    int compteur = 0;
    
    for (compteur = 0 ; compteur < 11 ; compteur = compteur + 1)
    {
        cout << compteur << endl;
    }
    
    double deuxPdix = pow(2, 10);
    
    cout << "2 a la puissance 10, ca fait " << deuxPdix << "." << endl;
    
    double racineC = sqrt(9);
    
    cout << "Et puis la racine carree de 9 c'est " << racineC << "." << endl;
    
    cout << "Enfin, cos(1) vaut " << cos(1) << " et sin(1) vaut " << sin(1) << " en degres" << endl;
    
    return 0;
}