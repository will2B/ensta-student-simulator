import sys
import re

class Token:
    def __init__(self,kind,value,pos):
        self.kind=kind
        self.value=value
        self.pos=pos


class Lexer:
    

    def __init__(self): #Notre methode constructeur
        self.stream = []
    
    def get_stream(self):
        return self.stream

    def set_stream(self, val):
        self.stream.append(val)

    def lex(self, lines, token_exprs):        
        tokens = []
        ligne = 0
        for characters in lines:
            pos = 0
            ligne += 1
            while pos < len(characters):
                match = None
                for token_expr in token_exprs:
                    pattern, tag = token_expr
                    regex = re.compile(pattern)
                    match = regex.match(characters, pos)
                    if match:
                        text = match.group(0)
                        if tag:
                            token = Token(tag, text, [ligne, pos])
                            tokens.append(token)
                        break
                if not match:
                    print 'Illegal character ligne ', ligne,"apres le charactere -", text,"- ( pos",pos,")\n"
                    sys.exit(1)
                else:
                    pos = match.end(0)
        return tokens



    def imp_lex(self, characters):

        token_exprs = [
                (r'[ \n\t]+',               None),
                #(r'#[^\n]*',               None),
                (r'\{',		               'laccolade'),
                (r'\}',		               'raccolade'),
                (r'\"',		               'guillemet'),
                (r'\?',		               'interro'),
                ('\'',		               'interro'),
				(r'!=',                    'different'),
                (r'\!',		               'exclamation'),
                (r'\#include',             'include'),
                (r'using',                 'using'),
                (r'namespace',             'namespace'),
                (r'\(',                    'lparen'),
                (r'\)',                    'rparen'),
                (r'\[',                    'lbracket'),
                (r'\]',                    'rbracket'),
                (r';',                     'semicolon'),
                (r':',                     'colon'),
                (r',',                     'comma'),
                (r'~',                     'tilde'),
                (r'\.',                    'dot'),
                (r'\+',                    'plus'),
                (r'-',                     'minus'),
                (r'\*',                    'multiply'),
                (r'/',                     'divide'),
                (r'<=',                    'inferiorEqual'),
                (r'<',                     'inferior'),
                (r'>=',                    'superiorEqual'),
                (r'>',                     'superior'),
                (r'==',                    'doubleEqual'),
                (r'=',                     'equal'),
                (r'DOUBLE|double',         'double'),
                (r'INT|int',               'int'),
                (r'ELSE IF|else if',       'else if'),
                (r'IF|if',                 'if'),
                (r'THEN|then',             'then'),
                (r'FOR|for',               'for'),
                (r'ELSE|else',             'else'),
                (r'WHILE|while',           'while'),
                (r'DO|do',                 'do'),
                (r'[0-9]+\.[0-9]+',        'doubleNumber'),
                (r'[0-9]+',                'integer'),
                (r'cos',                   'cos'),
                (r'sin',                   'sin'),
                (r'pow',                   'pow'),
                (r'sqrt',                  'sqrt'),
                (r'cout',                  'cout'),
                (r'cin',                   'cin'),
                (r'endl',                  'endl'),
                (r'main',                  'main'),
                (r'return',                'return'),
                (r'[A-Za-z][A-Za-z0-9_]*', 'ident'),
            ]


        return self.lex(characters, token_exprs)



    def tokenize(self, characters):
    	print "----------------------LEXER----------------------"
        tokens = self.imp_lex(characters)
        for token in tokens:
            self.stream.append(token)
        self.stream = self.stream[::-1]
        for i in self.stream:
            print i.kind, i.value
        return self.stream
            
    def get_next(self):
        return self.stream.pop()
        
    def show_next(self):
        return self.stream[-1]
        
    def lookahead(self, k):
        return self.stream[k]
        
    def getKind(self):
        return self.stream[1]

        
if __name__ == '__main__':
    lexer = Lexer()
    filename = sys.argv[1]
    file = open(filename)
    characters = file.readlines()
    file.close()
    lexer.tokenize(characters)
    print "\n\n"
    for token in lexer.get_stream():
        print token.value, token.kind, token.pos







