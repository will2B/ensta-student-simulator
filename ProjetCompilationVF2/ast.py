from visitor import Visitor

class Ast: #classe AST dont heritent toutes les sous-classes composant l'AST
    def __init__(self, name): #constructeur
        self.name = name

    # fonction qui utilise la metaprogrammation pour appeler les fonctions du visiteur
    def accept(self, visitor, arg):
        nom = self.__class__.__name__
        nomMethode = getattr(visitor, "visit" + nom)
        nomMethode(self, arg)

#toutes les classes seront construites sur le meme modele que celle-ci
class Identifier(Ast):
    def __init__(self, name): #constructeur
        self.name = name

    def __str__(self): #redefinition de la fonction print afin de pouvoir afficher un objet Identifier
        return "identifier - {0}".format(self.name)

class Operateur(Ast):
    def __init__(self, val):
        self.val = val

    def __str__(self):
        return "	operateur - {0}".format(self.val)

class Main(Ast):
    def __init__(self):
        self.type1 = None
        self.ident = []
        self.type2 = []
        self.statements = []

    def __str__(self):
        string = "Main\n {0}".format(self.type1)
        for i in range(len(self.ident)) :
        	string = string + "\n : {0}".format(self.type2[i])
        	string = string + "\n : {0}".format(self.ident[i])
        for stmt in self.statements :
        	string = string + "\n statement : {0}".format(stmt)
        return string

class Racine(Ast):
    def __init__(self):
        self.includes = []
        self.raccourcis = []
        self.main = []

    def __str__(self):
    	print "---------------------------AST-----------------------------"
        string = "Ast"
        for include in self.includes :
        	string = string + "\n {0}".format(include)
        for raccourci in self.raccourcis :
        	string = string + "\n {0}".format(raccourci)
        string = string + "\n {0}".format(self.main)
        return string

class Type(Ast):
    def __init__(self):
        self.val = None

    def __str__(self):
        return "type - {0}".format(self.val)

class Number(Ast):
    def __init__(self):
        self.type = None

    def __str__(self):
        return "number - {0}".format(self.type)

class Double(Ast):
    def __init__(self, val):
        self.val = val

    def __str__(self):
        return "double - {0}".format(self.val)

class Int(Ast):
    def __init__(self, val):
        self.val = val

    def __str__(self):
        return "int - {0}".format(self.val)

class Include(Ast):
    def __init__(self, val):
        self.val = val

    def __str__(self):
        return "include : {0}".format(self.val)

class Raccourci(Ast):
    def __init__(self, val):
        self.val = val

    def __str__(self):
        return "raccourci : {0}".format(self.val)

class Term(Ast):
    def __init__(self):
        self.val = None
        self.minus = None

    def __str__(self):
        return "term - {0}\n{1}".format(self.val, self.minus)

class Quote(Ast):
    def __init__(self, val):
        self.val = val

    def __str__(self):
        return "quote - "+self.val

class DeclareMethode(Ast):
    def __init__(self):
        self.type = None
        self.name = None
        self.fils = []

    def __str__(self):
        string = "methode, {0}, nom : {1} ".format(self.type, self.name)
        for son in self.fils :
            string = string + "\n    "+"arg - {0}, {1} ".format(son[0], son[1])
        return string

class Methode(Ast):
    def __init__(self):
        self.declaration = None
        self.statements = []

    def __str__(self):
        string = "{0}".format(self.declaration)
        for stmt in self.statements :
            string = string + "\n     "+"{0}".format(stmt)
        return string

class FonctionMath(Ast):
    def __init__(self, type1):
        self.type1 = type1
        self.args = []

    def __str__(self):
        string = "fonction - {0}".format(self.type1)
        for arg in self.args :
            string = string + "\n     "+"{0}".format(arg)
        return string

class FonctionPrint(Ast):
    def __init__(self, name):
        self.name = name
        self.args = []

    def __str__(self):
        string = "fonctionPrint "
        for arg in self.args :
            string = string + "\n     "+"{0}".format(arg)
        return string

class SimpleExpression(Ast):
    def __init__(self, val):
        self.val = val

    def __str__(self):
        return "simpleExpr \n  - {0}".format(self.val)

class Expression(Ast):
    def __init__(self):
        self.simpleExpr = []
        self.operateurs = []

    def __str__(self):
        string = "Expr "
        i=0
        for exp in self.simpleExpr :
            string = string + "\n     "+"{0}".format(exp)
            if(i<len(self.operateurs)):
                string = string + "\n     "+"{0}".format(self.operateurs[i])
            i+=1
        return string

class ExpressionC(Ast):
    def __init__(self):
        self.expr = None

    def __str__(self):
        return "expressionC - {0}".format(self.expr)

class Assignment(Ast):
    def __init__(self, type1):
        self.type1 = type1
        self.args=[]
        self.operateurs = []

    def __str__(self):
        string = "Assignment "+"{0}".format(self.type1)
        i=0
        for arg in self.args :
            string = string + "\n     "+"{0}".format(arg)
            if(i<len(self.operateurs)):
                string = string + "\n     "+"{0}".format(self.operateurs[i])
        return string


class IfStatement(Ast):
    def __init__(self, val):
        self.condition = val
        self.statements = []
        self.conditionElseIf = []
        self.statementsElseIf = []
        self.statementsElse = []

    def __str__(self):
        string = "if\n   condition - {0}".format(self.condition)
        for stmt in self.statements :
            string = string + "\n   - {0}".format(stmt)
        for i in range(len(self.conditionElseIf)) :
        	string = string + "\n  else if - {0}".format(self.conditionElseIf[i])
        	for stmt in self.statementsElseIf[i] :
        		string = string + "\n   - {0}".format(stmt)
        if (self.statementsElse != []):
            string = string + "\n  else"
        for stmt in self.statementsElse :
            string = string + "\n   - {0}".format(stmt)
        return string



class ForStatement(Ast):
    def __init__(self, stmt, exprC, expr):
        self.stmt = stmt
        self.exprC = exprC
        self.expr = expr
        self.statements = []

    def __str__(self):
        string = "for\n  {0} {1} {2}".format(self.stmt, self.exprC, self.expr)
        for stmt in self.statements :
            string = string + "\n   - {0}".format(stmt)
        return string

class WhileStatement(Ast):
    def __init__(self, expr):
        self.expr = expr
        self.statements = []

    def __str__(self):
		string = "while\n  - {0}".format(self.expr)
		for stmt in self.statements :
			string = string + "\n   - {0}".format(stmt)
		return string

class Return(Ast):
	def __init__(self, expr):
		self.expr = expr

	def __str__(self):
		return "return\n  - {0}".format(self.expr)








'''class ContitionFor1(Ast):
	def __init__(self):
        self.type1 = None
        self.exprC = None
        self.expr = None'''
